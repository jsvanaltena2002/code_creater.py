from Morse import morse
from Phone import phone

def Translate(translate_phrase, phone_morse, Encode_Decode):
    phrase_list = []
    for q in translate_phrase:
        phrase_list.append(q)
    # print(phrase_list)
    if phone_morse == "morse" and  Encode_Decode == "encode" :
        Trans_Morse_encode(phrase_list)
    elif phone_morse == "morse" and Encode_Decode == "decode" :
        Trans_Morse_Decode(phrase_list)
    elif phone_morse == "phone" and Encode_Decode == "encode":
        Trans_Phone_encode(phrase_list)
    elif phone_morse == "phone" and Encode_Decode == "Decode" :
        Trans_Phone_Decode(phrase_list)

def Trans_Morse_Decode (phrase_list):
    morse_answer = ""
    for l in phrase_list:
        for k in morse.values() :
            if l == k :
                morse_answer = morse_answer + " " + morse[l].key()
    print(morse_answer)

def Trans_Phone_Decode (phrase_list):
    phone_answer = ""
    for j in phrase_list:
        for h in phone.values():
            if j == h :
                phone_answer = phone_answer + " " + phone[h].key()
    print(phone_answer)

def Trans_Morse_encode (translate_phrase):
    Morse_Answer = ""
    for i in translate_phrase:
        for p in morse.keys() :
            if i == p :
                Morse_Answer = Morse_Answer + " " + morse[p]
    print(Morse_Answer)


def Trans_Phone_encode (translate_phrase) :
    Phone_Answer = ""
    for o in translate_phrase:
        for u in phone.keys() :
            if o == u:
                Phone_Answer = Phone_Answer + " " + phone[u]
    print(Phone_Answer)
