# from Morse import morse
# from Phone import phone
from Checks import Check_1 , Check_2 
from Translater import Translate

def StartUp():
    print("Hello!!\n" + 
          "How can I help you Encode today!!\n" +
          "Morse or Phone?")
    Morse_Phone = input("Please type here: ").lower()
    while  Check_1 (Morse_Phone) == False:
        Morse_Phone = input("Please type here: ").lower()

    print("\n" +
          "Would you like to Encode or Decode?\n" +
          "Encode - Change from normal words to Code\n" +
          "Decode - Change from Code to Normal words\n" +
          "Encode or Decode?")
    Encode_DeCode = input("Please type here: ").lower()
    counter = 0
    while Check_2 (Encode_DeCode) == False:
        counter =  counter + 1
        if counter == 15 :
            print(" Use this phrase to exit: cancel now" ) 
        Encode_DeCode = input("Please type here: ").lower()
    
    print("")

    if Encode_DeCode == "cancel now" :
        print("since you decided to cancel, press ctrl + c")
    else :
        print("What would you like to "+ Encode_DeCode + "?")
    phrase = input().lower()
    Answer(phrase , Morse_Phone, Encode_DeCode)

def Answer(phrase , phone_morse, Encode_Decode):
    # print(phrase)
    print(" Answer: ")
    Translate(phrase , phone_morse, Encode_Decode)
    # if phone_morse == "phone":
    #     print("")
    #     print(phone)
    # elif phone_morse == "morse":
    #     print("")
    #     print(morse)

if __name__ == "__main__":
    StartUp() 
